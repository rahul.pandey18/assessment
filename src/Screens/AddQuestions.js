import React, {Component} from 'react';
import ImagePicker from 'react-native-image-picker';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {postData} from '../Action/Index';
import {connect} from 'react-redux';

class AddQuestions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      content: '',
    };
  }

  submit = () => {

    console.warn(this.state);
    this.props.postData(this.state.title, this.state.content);
    this.setState({title: '', content: ''});

    this.props.navigation.navigate('Listing',{imageUri:this.state.avatarSource.uri });
    
  };



ImageFunction= ()=>{
    const options = {
        title: 'Select Avatar',
        customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      };
      
      /**
       * The first arg is the options object for customization (it can also be null or omitted for default options),
       * The second arg is the callback which sends object: response (more info in the API Reference)
       */
      ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);
      
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          const source = { uri: response.uri };
      
          // You can also display the image using data:
          // const source = { uri: 'data:image/jpeg;base64,' + response.data };
      
          this.setState({
            avatarSource: source,
          });


          console.warn("Image Data",this.state.avatarSource)
        }
      });
}



  render() {

    return (
      <TouchableWithoutFeedback
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={{flex: 1, backgroundColor: ''}}>
          {/* 
Header */}

          <View
            style={{
              flex: 1,
              backgroundColor: '',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginBottom: wp('5%'),
            }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Listing')}>
              <Image
                source={require('../Assets/Back.png')}
                style={{
                  resizeMode: 'contain',
                  height: wp('10%'),
                  width: wp('10%'),
                }}
              />
            </TouchableOpacity>
            <Text style={{fontSize: 25, marginRight: wp('50%')}}>
              Add Questions
            </Text>
          </View>
          <View style={{flex: 10, backgroundColor: ''}}>
            <View style={{flex: 1, margin: wp('5%'), backgroundColor: ''}}>
              {/* Ttile Portion..................... */}

              <View
                style={{
                  height: hp('10%'),
                  backgroundColor: 'white',
                  borderWidth: 1,
                  marginBottom: wp('5%'),
                }}>
                <View style={{flex: 1.5, backgroundColor: ''}}>
                  <Text style={{fontSize: 20, letterSpacing: 0.5}}>Title</Text>
                </View>
                <View style={{flex: 2, backgroundColor: ''}}>
                  <TextInput
                    placeholder="Type Here ...    "
                    onChangeText={title => this.setState({title})}
                    value={this.state.title}
                  />
                </View>
              </View>

              {/* Content Box............... */}

              <View
                style={{
                  height: hp('20%'),
                  backgroundColor: 'white',
                  borderWidth: 1,
                  marginBottom: wp('5%'),
                }}>
                <View style={{flex: 1.5, backgroundColor: ''}}>
                  <Text style={{fontSize: 20, letterSpacing: 0.5}}>
                    Content
                  </Text>
                </View>
                <View style={{flex: 3, backgroundColor: ''}}>
                  <TextInput
                    placeholder="Type Here ...    "
                    multiline={true}
                    onChangeText={content => this.setState({content})}
                    value={this.state.content}
                  />
                </View>
              </View>

              <View
                style={{
                  height: hp('10%'),
                  backgroundColor: '',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: wp('10%'),flexDirection:"row"
                }}>
                <TouchableOpacity onPress={this.ImageFunction}>
                  <Image
                    source={require('../Assets/download.png')}
                    style={{
                      resizeMode: 'contain',
                      height: wp('15%'),
                      width: wp('15%'),
                    }}
                  />
                </TouchableOpacity>
               
                {this.state.avatarSource ? (<Image
                source={{uri:this.state.avatarSource.uri }}
                style={{height:wp("20%"),width:wp("20%")}}
                />): null}

              </View>

              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <TouchableOpacity onPress={this.submit}>
                  <View
                    style={{
                      height: hp('7%'),
                      width: wp('70%'),
                      backgroundColor: 'green',
                      borderRadius: 30,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{fontSize: 20, color: 'white'}}>Save</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export default connect(null, {postData})(AddQuestions);
