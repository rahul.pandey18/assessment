import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {getData} from '../Action/Index';
import {connect} from 'react-redux';
import _ from 'lodash';
class Listing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      comingData: {},
      status:false,
      filterSearch:[]
    };
  }

  componentDidMount() {
    this.props.getData();
    this.setState({comingData: this.props.getListOfData});
    console.log(this.state.comingData)
    if(this.props.getListOfData){
      this.setState({status:true})
    }
  }

componentDidUpdate=()=>{
   
}


clearText=()=>{
    this.setState({search:''})
}
 

searchFunction=(text)=>{
  // const data= this.props.getListOfData.filter((title)=>{
  //   if(this.state.search === title){
  //     console.warn(this.state.filterSearch)
  //   }




  // })


  // console.warn("-------data------",data)


  console.warn("---------working--------")

}

  render() {
     console.warn('Coming data ', this.props.getListOfData.title);
   const ImageUrl= this.props.navigation.getParam("imageUri", "data")
   console.log("Imagedata",ImageUrl)
  //   console.log('Coming data in Array ', this.state.comingData);
    return (
      <View style={{flex: 1, backgroundColor: ''}}>
        <View
          style={{
            flex: 1,
            backgroundColor: '',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={()=>{
            alert("Don't Press Please !!")
          }}>
          <Image
            source={require('../Assets/Back.png')}
            style={{resizeMode: 'contain', height: wp('10%'), width: wp('10%')}}
          />
          </TouchableOpacity>

          <Text style={{fontSize: 25, marginRight: wp('80%')}}>Listing</Text>
        </View>
        <View style={{flex: 10, backgroundColor: ''}}>
          <View
            style={{
              height: hp('10%'),
              backgroundColor: '',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                height: hp('8%'),
                width: wp('90%'),
                backgroundColor: '',
                flexDirection: 'row',
                borderWidth: 1,
                borderColor: 'grey',
              }}>
              <View
                style={{
                  flex: 1,
                  backgroundColor: '',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
               
               <TouchableOpacity onPress={this.searchFunction}>
               <Image
                  source={require('../Assets/Search.jpeg')}
                  style={{
                    resizeMode: 'contain',
                    height: wp('8%'),
                    width: wp('8%'),
                  }}
                />
               </TouchableOpacity>
              </View>
              <View
                style={{
                  flex: 8,
                  backgroundColor: '',
                  justifyContent: 'center',
                }}>
                <TextInput
                  placeholder="Type here..."
                  onChangeText={search => this.setState({search})}
                  value={this.state.search}
                />
                {}
              </View>
              <View
                style={{
                  flex: 1,
                  backgroundColor: '',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity onPress={this.clearText}>
                  <Image
                    source={require('../Assets/Cross.jpeg')}
                    style={{
                      resizeMode: 'contain',
                      height: wp('8%'),
                      width: wp('8%'),
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          {/* content */}
          <View style={{flex: 1, backgroundColor: ''}}>
            <View
              style={{
                flex: 1,
                backgroundColor: '',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('AddQuestions')}>
                <Image
                  source={require('../Assets/plus.jpeg')}
                  style={{
                    resizeMode: 'contain',
                    height: wp('5%'),
                    width: wp('5%'),
                    marginLeft: wp('80%'),
                  }}
                />
              </TouchableOpacity>
            </View>
            <View style={{flex: 10, backgroundColor: '', margin: wp('5%')}}>
              {/* Details box */}

              {/* 
<View style={{height:hp("12%"),backgroundColor:"white",borderRadius:10,margin:wp("1%"),flexDirection:"row",marginBottom:wp("5%")}}>

<View style={{flex:1,backgroundColor:"",justifyContent:"center",alignItems:"center"}}>

<View style={{borderWidth:1,borderRadius:10,borderColor:"gray"}}>
<Image
source={require("../Assets/User.png")}
style={{resizeMode:"contain",height:wp("20%"),width:wp("20%"),borderRadius:15,tintColor:"red",}}
/>
</View>
</View>
<View style={{flex:2,backgroundColor:"white",borderRadius:10,justifyContent:"center"}}>
    <Text style={{fontSize:20}}>Rahul Pandey</Text>
</View>
</View> */}

              {/* ///////////////////////////////////////////////////////////// */}



{
this.state.status === false ? (
<View style={{justifyContent:"center",alignItems:"center",backgroundColor:""}}>
<ActivityIndicator
  size={70} color="black"
  />
  
</View>
  
    ):(


      
    <FlatList
    data={this.props.getListOfData}
    keyExtractor={item => {
      item.key;
    }}
    renderItem={({item}) => {
      return (
        <View
          style={{
            height: hp('12%'),
            backgroundColor: 'white',
            borderRadius: 10,
            margin: wp('1%'),
            flexDirection: 'row',
            marginBottom: wp('5%'),
            borderWidth:1
          }}>
          <View
            style={{
              flex: 1,
              backgroundColor: '',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                borderWidth: 1,
                borderRadius: 10,
                borderColor: 'gray',
              }}>
            

{ImageUrl ? (<Image
    source={{uri:ImageUrl}}
    style={{height:wp("20%"),width:wp("20%"),  borderRadius: 15,}}
    />):   (<Image
        source={require('../Assets/User.png')}
 
        style={{
          resizeMode: 'contain',
          height: wp('20%'),
          width: wp('20%'),
          borderRadius: 15,
          tintColor: 'red',
        }}/>)
  }


            </View>
          </View>
          <View
            style={{
              flex: 2,
              backgroundColor: '',
              borderRadius: 10,
              
              
            }}>
            <View style={{flexDirection:"row",justifyContent:"flex-start",alignItems:"center",backgroundColor:"",height:hp("4%"),width:wp("55%")}}>
           
            <Text style={{fontSize: 20}}>{item.title}</Text>
            </View>

           
          </View>
        </View>
      );
    }}
  />

  ) 



}



             




            </View>
          </View>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  const getListOfData = _.map(state.getList.getList, (val, key) => {
    return {
      ...val,
      key: key,
    };
  });

  return {
    getListOfData,
  };
}

export default connect(mapStateToProps, {getData})(Listing);
