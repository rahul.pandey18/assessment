import firebase from '../Firebase/Fb'



export  function getData(){
    return (dispatch)=>{
        firebase.database().ref('/data').on('value',snapshot=>{    //for fetch blog from firebase
            dispatch({
                type:"FETCH_DATA",
                payload:snapshot.val()
            })


        })

    }
}



export function postData(title,content){
    return(dispatch)=>{
        firebase.database().ref('/data').push({title,content})    //for posting data to database
    }

}