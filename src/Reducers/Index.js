import {combineReducers} from 'redux';
import PostReducers from './PostReducer'

const rootReducers = combineReducers({
  getList: PostReducers,
});

export default rootReducers;
