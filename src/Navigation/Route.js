
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Listing from '../Screens/Listing'
import AddQuestions from '../Screens/AddQuestions'

const AppNavigator = createStackNavigator({
  Listing: {
    screen: Listing,
    navigationOptions: {
        headerShown: false
      },
  },
 
  AddQuestions: {
    screen: AddQuestions,
    navigationOptions: {
        headerShown: false
      },
  },
},
{
  initialRouteName: 'Listing',
},);

export default  createAppContainer(AppNavigator);