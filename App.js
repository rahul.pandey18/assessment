import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import ReduxThunk from 'redux-thunk'
import {Provider} from 'react-redux'
import {createStore,applyMiddleware} from 'redux'
import reducers from "./src/Reducers/Index"
import Route from '../Assessment_2/src/Navigation/Route'

export default class App extends Component {
  render() {

    const store = createStore(reducers,{},applyMiddleware(ReduxThunk))

    return  (
      <Provider store={store}>
        <Route/>
      </Provider>
    )
  }
}














